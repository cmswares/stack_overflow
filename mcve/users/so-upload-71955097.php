<form action="so-upload-71955097" method="post" enctype="multipart/form-data">
  Select file to upload:
  <input type="file" name="file" />
  <input type="submit" value="Upload File" name="submit" />
</form>

<?php
/* 
	MCVE for:	https://stackoverflow.com/questions/71955097/how-to-show-result-after-uploaded-file-in-php
*/

//upload.php

$folder_name = 'tmp/';
// $tumb_name = 'thumb/';
// $imageext = '.png';

if(!empty($_FILES)) {

	$temp_file = $_FILES['file']['tmp_name'];
	$location = $folder_name . $_FILES['file']['name'];
	move_uploaded_file($temp_file, $location);
	$upload = $_FILES['file']['name'];
	$uploadStr = str_replace(" ", "\ ",$upload);
	$locationStr = str_replace(" ","\ ",$location);
	// $cmd  = "ffmpeg -y -i {$locationStr} -ss 00:00:15 -vframes 1 thumb/{$uploadStr}.png 2>&1";
	// echo shell_exec($cmd);
	
	// These lines need to be *INSIDE* the "uploading" condition
	
	echo "The file " . $location . " has been uploaded";
	// not working
	echo "<br>";
	echo "The file " . $upload . " has been uploaded";
	// not working
	echo "<br>";
	echo "The file " . $uploadStr . " has been uploaded";
	// not working
	echo "<br>";
	echo "The file " . $locationStr . " has been uploaded";
}

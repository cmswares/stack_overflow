<?php declare(strict_types=1);

/**
 *	Get Dates In Month.
 *	
 *	@updated 2022-12-15 19:38:46 +07:00
 *
 *	@param bool|string	$yearmonth
 *						true = current year/month
 *						string = other year/month
 *	
 *	@note If using PHP < 8, remove the bool|string union type signature!
 *  @source https://bitbucket.org/cmswares/stack_overflow/src/master/functions/func.get_dates_in_month.php
 *	@ref https://stackoverflow.com/questions/74809988/how-can-i-get-all-dates-of-current-month-till-current-date
 *	
 *	@return array
 */

function get_dates_in_month(bool|string $yearmonth): array
{	
	/* If for current year/month */
	if($yearmonth === true) {
		$base = date('Y-m'); // Current base date
		$days = range(1,  date('d')); // Days from 1 to now
	} 
	/* If for other year/month */
	else {
		$base = $yearmonth; // Literal base date
		$days = range(1,  date('t', strtotime($yearmonth))); // Days from 1 to  month max
	}

	/* Combine with day added */
	$dates = array_map(function($day) use ($base) {
		return $base . '-' . str_pad((string) $day, 2, "0", STR_PAD_LEFT);
	}, $days);
	return $dates;
}

/* Usage:  */

echo "<pre>";

echo "==== Current Y/M ====\n";
var_dump(get_dates_in_month(true));

echo "==== Other Y/M ====\n";
var_dump(get_dates_in_month('1980-02'));

echo "</pre>";

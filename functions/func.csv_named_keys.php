<?php declare(strict_types=1);

/**
 *	Csv Named Keys.
 *	
 *	@updated 2022-12-15 20:50:56 +07:00
 *
 *	@param array	$array
 *	@param int		$index = null
 *	
 *	First argument $array is the source array itself. 
 *	Second argument $index (optional) is the index (numeric) for the value that should be used as the index of the resulting array. If null, row numbers from source array (starting from 1) are used.
 *	
 *	@ref https://stackoverflow.com/questions/71900912/convert-array-from-spreadsheet-into-associative-array-with-header-row-as-keys
 *	@return array
 */

function csv_named_keys(array $array, ?int $index = null): array 
{
    // Shift the keys from the first row: 
    $keys = array_shift($array);

    $named = [];
    // Loop and build remaining rows into a new array:
    
    foreach($array as $ln => $vals) {

        // Using specific index or row numbers?
        $key = !is_null($index) ? $vals[$index] : $ln;

        // Combine keys and values:
        $named[$key] = array_combine($keys, $vals);
    }

    return $named;
}

/* ==== USAGE ==== */

$raw = [['a', 'b'], [111,'foo'], [333,'bar'], [555,'nix']];

$named = csv_named_keys($raw);
$named_key_a = csv_named_keys($raw, 0);

echo '<pre>';
var_dump($named);
var_dump($named_key_a);

/* 
	results in:

	array(3) {
		[111] · array(2) {
			["a"] · int(111)
			["b"] · string(3) "foo"
		}
		[333] · array(2) {
			["a"] · int(333)
			["b"] · string(3) "bar"
		}
		[555] · array(2) {
			["a"] · int(555)
			["b"] · string(3) "nix"
		}
	} 
*/

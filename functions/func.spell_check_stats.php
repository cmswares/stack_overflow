<?php

/**
 *	Spell Check Stats.
 *	Returns an array with OK, FAIL spell check counts and their ratio.
 *	Use the ratio to filter out undesirable (non-English/garbled) content.
 *	
 *	@updated 2022-12-29 00:00:29 +07:00
 *	@author @cmswares
 *	@ref https://stackoverflow.com/q/74910421/4630325
 *
 *	@param string	$text
 *	
 *	@return array
 */

function spell_check_stats(string $text): array
{
	$stats = [
		'ratio' => null,
		'ok' => 0,
		'fail' => 0
	];
	
	// Split into words
	$words = preg_split('~[^\w\']+~', $text, -1, PREG_SPLIT_NO_EMPTY);
	
	// Nw PSpell:
	$pspeller = pspell_new("en");
	
	// Check spelling and build stats
	foreach($words as $word) {
		if(pspell_check($pspeller, $word)) {
			$stats['ok']++;
		} else {
			$stats['fail']++;
		}
	}
	
	// Calculate ratio of OK to FAIL
	$stats['ratio'] = $stats['ok'] / $stats['fail'];

	return $stats;
}


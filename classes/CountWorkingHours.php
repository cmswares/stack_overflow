<?php declare(strict_types=1);

/**
 *	Count Working Hours.
 *	
 *	@updated 2022-12-15 20:39:06 +07:00
 *	
 *	1. Configure how many hours should be counted for for each day;
 *	2. Create an iterable DatePeriod (with DateTime objects for each date in the period);
 *	3. Iterate dates, look up how many hours should be counted for each day, sum it up.
 *	
 *	Returns an integer with working hours in a given period:
 *	@ref https://stackoverflow.com/questions/71979906/counting-working-hours-different-for-weekdays-and-saturdays
 */

class CountWorkingHours
{
	// Define hours counted for each day:
	public array $hours = [ 
		'Mon' => 8, 
		'Tue' => 8, 
		'Wed' => 8, 
		'Thu' => 8, 
		'Fri' => 8, 
		'Sat' => 3, 
		'Sun' => 0
	];

	
	/**
	 *	Get Hours For Period.
	 *	
	 *	@updated 2022-12-15 20:45:21 +07:00
	 *
	 *	@param string	$from
	 *	@param string	$to
	 *	
	 *	@return int
	 */
	
	public function get_hours_for_period(string $from, string $to): int
	{
		// Create DatePeriod with requested Start/End dates:
		$period = new DatePeriod(
			new DateTime($from), 
			new DateInterval('P1D'), 
			new DateTime($to)
		);
		
		$hours = [];

		// Loop over DateTime objects in the DatePeriod:
		foreach($period as $date) {
			// Get name of day and add matching hours:
			$day = $date->format('D');
			$hours[] = $this->hours[$day];
		}
		// Return sum of hours:
		return array_sum($hours);
	}
}

/* ==== USAGE ==== */
// Returns an integer with working hours in a given period:

$cwh = new CountWorkingHours();
$hours = $cwh->get_hours_for_period('2022-04-21 07:00:00', '2022-04-30 16:00:00');
var_dump($hours);
// = 62
